DATA_B28_8000:
.dw DATA_B28_8030
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_803E
.dw DATA_B28_804C 
.dw DATA_B28_8057
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_805F 
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036
.dw DATA_B28_8036

DATA_B28_8030:
.db $E0, $00  
	.dw LABEL_B28_8067
.db $FF, $00

DATA_B28_8036:
.db $FF, $07
	.dw LABEL_B28_81EE
	.dw LABEL_B28_8080
.db $FF, $00

DATA_B28_803E:
.db $FF, $04
	.dw $0000
	.dw $0000
.db $FF, $07
	.dw LABEL_B28_81EE
	.dw LABEL_B28_80F6
.db $FF, $00

DATA_B28_804C:
.db $FF, $07
	.dw LABEL_B28_81E7
	.dw LABEL_B28_8104
.db $FF, $05
	.db $0A
.db $FF, $00

DATA_B28_8057:
.db $FF, $07
	.dw LABEL_B28_81EE
	.dw MovingPlatform_FallDown
.db $FF, $00

DATA_B28_805F:
.db $FF, $07
	.dw LABEL_B28_81EE
	.dw LABEL_B28_8123
.db $FF, $00

LABEL_B28_8067:
	bit     6, (ix+$04)
	ret     nz
	set     7, (ix+$03)
	ld      a, (ix+$3F)
	and     $0F
	rlca    
	rlca    
	rlca    
	or      a
	jr      nz, +
	inc     a
+:	ld      (ix+$02), a
	ret     

LABEL_B28_8080:
	call    LABEL_200 + $39
	ld      a, (ix+$22)
	and     $0C
	jr      z, LABEL_B28_809B
	and     $04
	rlca    
	rlca    
	ld      b, a
	ld      a, (ix+$0A)
	and     $EF
	or      b
	ld      (ix+$0A), a
	jp      LABEL_B28_80D6

LABEL_B28_809B:
	ld      a, (ix+$1b)
	or      a
	jp      z, LABEL_B28_80D6
	ld      hl, ($D174)		;add 128 to the horizontal position
	ld      de, $0080
	add     hl, de
	ld      e, (ix+$11)
	ld      d, (ix+$12)
	xor     a
	sbc     hl, de
	jr      c, LABEL_B28_80C3
	ld      de, $00C0
	xor     a
	sbc     hl, de
	jr      c, LABEL_B28_80D6
	res     4, (ix+$0A)
	jp      LABEL_B28_80D6

LABEL_B28_80C3:
	dec     hl
	ld      a, h
	cpl     
	ld      h, a
	ld      a, l
	cpl     
	ld      l, a
	ld      de, $00C0
	xor     a
	sbc     hl, de
	jr      c, LABEL_B28_80D6
	set     4, (ix+$0A)
LABEL_B28_80D6:
	ld      a, $01
	bit     4, (ix+$0A)
	jr      z, +
	neg     
+:	ld      (ix+$17), a
	call    MovingPlatform_HandlePlayerCollision
	ld      a, (CurrentLevel)
	cp      $04
	ret     nz
	bit     6, (ix+$04)
	ret     z
	ld      (ix+$00), $FF
	ret     

LABEL_B28_80F6:
	call    MovingPlatform_HandlePlayerCollision
	ld      a, (ix+$21)
	and     $0F
	ret     z
	ld      (ix+$02), $09
	ret     

LABEL_B28_8104:
	call    MovingPlatform_HandlePlayerCollision
	ret     


MovingPlatform_FallDown:                                            ; $8108
        ; move down until the object falls off screen then delete it.
	set     OBJ_F4_BIT0, (ix + Object.Flags04)
	ld      de, $0020
	ld      bc, $0600
	call    VF_Engine_SetObjectVerticalSpeed
	call    MovingPlatform_HandlePlayerCollision
	ld      a, (ix + Object.ScreenY)
	cp      $C0
	ret     c
	ld      (ix + Object.ObjID), Object_Dummy
	ret     

LABEL_B28_8123:
	ld      (ix+$0b), $30
	call    LABEL_B28_8131
	call    LABEL_200 + $36
	call    MovingPlatform_HandlePlayerCollision
	ret     

LABEL_B28_8131:
	ld      a, (ix+$0A)
	or      a
	jp      z, LABEL_B28_8147
	ld      a, (ix+$1E)
	cpl     
	and     $01
	add     a, a
	dec     a
	add     a, (ix+$0a)
	ld      (ix+$0a), a
	ret     

LABEL_B28_8147:
	ld      (ix+$0a), $80
	inc     (ix+$1e)
	ret     


MovingPlatform_HandlePlayerCollision:                               ; $814F
	ld      a, ($d521)
	ld      ($D100), a
	call    VF_Engine_CheckCollision
	ld      a, (ix + Object.SprColFlags)

        ; jump if no collisions or collision at the bottom edge
	and     $0F
	jr      z, LABEL_B28_819B
	bit     OBJ_COL_BOTTOM_BIT, a
	jr      nz, LABEL_B28_819B
        ; jump if sonic is moving up (jumping)
	ld      a, (Player.VelY + 1)
	rla     
	jr      c, LABEL_B28_819B
        ; jumo if sonic is already standing on something
	ld      a, (Player.BgColFlags)
	bit     1, a
	jr      nz, LABEL_B28_819B

	call    VF_Engine_UpdateObjectPosition
        
        ; flag this object and the player to indicate a collision
	ld      (ix + Object.SprColFlags), OBJ_COL_TOP
	ld      a, $20
	ld      (Player.SprColFlags), a
	ld      a, (Player.ix23)
	set     OBJ_F23_BIT1, a
	ld      (Player.ix23), a

	push    ix
	pop     hl
	call    VF_Engine_GetObjectIndexFromPointer
	ld      b, a
	ld      a, ($d3b9)
	or      a
	jr      z, +
	cp      b
	ret     nz
+:	ld      a, b
	ld      ($d3b9), a
	call    MovingPlatform_AdjustPlayerPosition
	ret     

LABEL_B28_819B:
	call    VF_Engine_UpdateObjectPosition
	ld      (ix+$21), $00
	ld      a, ($d100)
	ld      ($d521), a
	ld      a, ($d3b9)
	ld      b, a
	push    ix
	pop     hl
	call    VF_Engine_GetObjectIndexFromPointer
	cp      b
	ret     nz
	xor     a
	ld      ($d3b9), a
	ret     


MovingPlatform_AdjustPlayerPosition:                                ; $81B9
        ; move the player to stand on the platform
	ld      e, (ix + Object.Height)
	ld      d, $00
	ld      l, (ix + Object.Y)
	ld      h, (ix + Object.Y + 1)
	inc     hl
	xor     a
	sbc     hl, de
	ld      (Player.Y), hl

        ; if this platform is moving horizontally move the player to match
	ld      hl, (Player.SubPixelX)
	ld      e, (ix + Object.VelX)
	ld      d, (ix + Object.VelX + 1)
	ld      a, (Player.X + 1)
	ld      b, a
	ld      a, d
	and     $80
	rlca    
	dec     a
	cpl     
	add     hl, de
	adc     a, b
	ld      (Player.X + 1), a
	ld      (Player.SubPixelX), hl
	ret     


LABEL_B28_81E7:
	ld      (ix+$07), $20
	jp      LABEL_B28_81F2

LABEL_B28_81EE:
	ld      (ix+$07), $e0
LABEL_B28_81F2:
	ld      a, (ix+$3f)
	and     $f0
	rlca    
	rlca    
	rlca    
	rlca    
	inc     a
	ld      (ix+$06), a
	ret     


Logic_MovingPlatform2:                                              ; $8200
.dw MovingPlatform2_State_Init
.dw MovingPlatform2_State_GMZ3_1
.dw MovingPlatform2_State_GMZ3_2
.dw MovingPlatform2_State_SEZ2_1
.dw MovingPlatform2_State_SEZ2_2
.dw MovingPlatform2_State_Idle


MovingPlatform2_State_Init:                                         ; $820C
.db $01, $01
	.dw MovingPlatform2_Init
.db $FF, $00


; first GMZ3 platform
MovingPlatform2_State_GMZ3_1:                                       ; $8212
.db $FF, $04                    ; move up for 384 frames
	.dw $0000
	.dw $FF00
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04                    ; move down for 384 frames
	.dw $0000
	.dw $0100
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $05                    ; reinitialise this object
	.db $00
.db $00, $01
	.dw VF_DoNothing
.db $FF, $00


; second GMZ3 platform
MovingPlatform2_State_GMZ3_2:                                       ; $823F
.db $FF, $04                    ; move up for 208 frames
	.dw $0000
	.dw $FF00
.db $D0, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; move right for 96 frames
	.dw $0100
	.dw $0000
.db $60, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; move up for 224 frames
	.dw $0000
	.dw $FF00
.db $E0, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; move down for 96 frames
	.dw $0000
	.dw $0100
.db $60, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; move left for 384 frames
	.dw $FF00
	.dw $0000
.db $80, $01
	.dw LABEL_B28_836A
.db $80, $01
	.dw LABEL_B28_836A
.db $80, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; move down for 32 frames
	.dw $0000
	.dw $0100
.db $20, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; move up for 352 frames
	.dw $FF00
	.dw $0000
.db $80, $01
	.dw LABEL_B28_836A
.db $80, $01
	.dw LABEL_B28_836A
.db $60, $01
	.dw LABEL_B28_836A
.db $FF, $04                    ; stop for 32 frames frames then fall down
	.dw $0000
	.dw $0000
.db $20, $01
	.dw LABEL_B28_836A
.db $80, $01
	.dw MovingPlatform_FallDown
.db $80, $01
	.dw MovingPlatform_FallDown
.db $80, $01
	.dw MovingPlatform_FallDown
.db $FF, $05
	.db $00
.db $00, $01
	.dw VF_DoNothing
.db $FF, $00


MovingPlatform2_State_SEZ2_1:                                       ; $82B4
.db $FF, $04
	.dw $0280
	.dw $0000
.db $CC, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $FD80
	.dw $0000
.db $CC, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $05
	.db $00
.db $00, $01
	.dw VF_DoNothing
.db $FF, $00


; second SEZ2 platform
MovingPlatform2_State_SEZ2_2:                                       ; $82D1
.db $FF, $04
	.dw $0220
	.dw $0000
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $78, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $0000
	.dw $FF00
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $FE00
	.dw $0000
.db $08, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $F000
	.dw $0000
.db $07, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $0000
	.dw $F400
.db $08, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $FF00
	.dw $0000
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $0000
	.dw $FF00
.db $A0, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $FF00
	.dw $0000
.db $80, $01
	.dw MovingPlatform_HandleCollisions
.db $A0, $01
	.dw MovingPlatform_HandleCollisions
.db $FF, $04
	.dw $0000
	.dw $0000
.db $20, $01
	.dw MovingPlatform_HandleCollisions
.db $80, $01
	.dw MovingPlatform_FallDown
.db $80, $01
	.dw MovingPlatform_FallDown
.db $80, $01
	.dw MovingPlatform_FallDown
.db $FF, $05
    .db $00
.db $00, $01
	.dw VF_DoNothing
.db $FF, $00


MovingPlatform2_State_Idle:                                         ; $834C
.db $06, $01
	.dw MovingPlatform2_Idle
.db $FF, $00


MovingPlatform2_Init:                                               ; $8352
	set     7, (ix + Object.Flags03)
	ld      a, (ix + Object.ix3F)
	dec     a
	jr      z, +
	bit     6, (ix + Object.Flags04)
	ret     nz
	set     1, (ix + Object.Flags04)
+:	ld      (ix + Object.StateNext), $05
	ret

LABEL_B28_836A:
	call    MovingPlatform_HandlePlayerCollision
	res     OBJ_F4_BIT1, (ix + Object.Flags04)
	res     OBJ_F4_VISIBLE, (ix + Object.Flags04)
	bit     OBJ_F4_BIT6, (ix + Object.Flags04)
	ret     z
	ld      (ix + Object.StateNext), $05
	ret     

MovingPlatform_HandleCollisions:                                    ; $837F
	call    MovingPlatform_HandlePlayerCollision
	ret     


MovingPlatform2_Idle:                                               ; $8383
	ld      a, (ix + Object.ix3F)
	dec     a
	jr      z, +
	bit     OBJ_F4_BIT6, (ix + Object.Flags04)
	ret     nz
+:	ld      a, $00
	ld      (ix + Object.VelX), a
	ld      (ix + Object.VelX + 1), a
	ld      (ix + Object.VelY), a
	ld      (ix + Object.VelY + 1), a
	ld      a, (ix + Object.InitialX + 1)
	ld      (ix + Object.X + 1), a
	ld      a, (ix + Object.InitialX)
	ld      (ix + Object.X), a
	ld      a, (ix + Object.InitialY + 1)
	ld      (ix + Object.Y + 1), a
	ld      a, (ix + Object.InitialY)
	ld      (ix + Object.Y), a
	call    MovingPlatform_HandlePlayerCollision
	ld      a, (ix + Object.SprColFlags)
	and     $0F
	ret     z
	ld      a, (ix + Object.ix3F)
	inc     a
	ld      (ix + Object.StateNext), a
	ret     


DATA_B28_83C5:
.dw DATA_B28_83C9
.dw DATA_B28_83DD

DATA_B28_83C9:
.db $E0, $00
	.dw LABEL_B28_83CF
.db $FF, $00


LABEL_B28_83CF:
	bit     6, (ix+$04)
	ret     nz
	ld      (ix+$02), $01
	set     7, (ix+$03)
	ret     

DATA_B28_83DD:
.db $E0, $01
	.dw LABEL_B28_83E3
.db $FF, $00

LABEL_B28_83E3:
	ld      hl, $0100
	bit     0, (ix+$1e)
	jr      z, +
	ld      hl, $ff00
+:	ld      (ix+$16), l
	ld      (ix+$17), h
	call    MovingPlatform_HandlePlayerCollision
	ld      l, (ix+$11)
	ld      h, (ix+$12)
	ld      e, (ix+$3a)
	ld      d, (ix+$3b)
	xor     a
	sbc     hl, de
	jr      nc, LABEL_B28_840E
	res     0, (ix+$1e)
	ret     

LABEL_B28_840E:
	ld		a, h
	or		a
	jr		nz, $05
	ld		a, l
	cp		(ix+$3F)
	ret		c
	set		0, (ix+$1E)
	ret



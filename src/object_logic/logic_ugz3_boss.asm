; index of the main boss object (robotnik)
.def UGZ3_Robotnik_BossIndex        $D46A
; index of the pincer object
.def UGZ3_Robotnik_PincersIndex     $D46B

; the number of hits required to destroy the pincer object
.def UGZ3_PincerNumberOfHits        $06
; the index of the hit counter in the pincer object
.def UGZ3_PincerHitCounter          $24
; the index of the hurt state counter in the pincer object
.def UGZ3_Pincer_HurtStateCounter   $1E


Logic_UGZ3_Robotnik:			;$A39D
.dw UGZ3_Robotnik_State_00
.dw UGZ3_Robotnik_State_MoveToPlayerX
.dw UGZ3_Robotnik_State_MoveToGrabPlayer
.dw UGZ3_Robotnik_State_MoveOverLedge
.dw UGZ3_Robotnik_State_MovePlayerDown
.dw UGZ3_Robotnik_State_FlyAway
.dw UGZ3_Robotnik_State_SpawnCannonBall
.dw UGZ3_Robotnik_State_DropOntoPincers
.dw UGZ3_Robotnik_State_08
.dw UGZ3_Robotnik_State_09
.dw UGZ3_Robotnik_State_LiftPlayer
.dw UGZ3_Robotnik_State_GrabPlayer


; =============================================================================
;  Cannon Ball Object Logic
; -----------------------------------------------------------------------------
Logic_UGZ3_CannonBall:			;$A3B5
.dw UGZ3_CannonBall_State_00
.dw UGZ3_CannonBall_State_01

UGZ3_CannonBall_State_00:		;$A3B9
.db $FF, $02
	.dw UGZ3_CannonBall_Init
.db $FF, $05		;change sprite state to $01
	.db $01
.db $FF, $03

UGZ3_CannonBall_State_01:		;$A3C2
.db $E0, $01
	.dw UGZ3_CannonBall_MainLoop
.db $FF, $00


UGZ3_CannonBall_Init:		;$A3C8
        ; set the flag to indicate that a cannon ball is present
	ld      a, $FF
	ld      ($D4A2), a
        ; set horizontal speed to 256 + number of seconds on the timer
	ld      hl, $0100
	ld      a, (LevelTimer_Seconds)
	ld      e, a
	ld      d, $00
	add     hl, de
	ld      (ix + Object.VelX), l
	ld      (ix + Object.VelX + 1), h
	ret     


UGZ3_CannonBall_MainLoop:		;$A3DE
        ; check collisions with the background tiles
	call    LABEL_200 + $60
        ; test object collisions
	call    LABEL_200 + $1B
	ld      a, (ix + Object.SprColFlags)
	and     $0F
	jp      z, +

        ; colliding with player - set the hurt flag
	ld      a, $FF
	ld      (Player_HurtTrigger), a
	jp      VF_Engine_DisplayExplosionObject

+:	;update speed + position
        call    VF_Engine_UpdateObjectPosition
	ld      de, $0060		;value to adjust by
	ld      bc, $0600		;maximum velocity
	call    VF_Engine_SetObjectVerticalSpeed

        ; check to see if the cannon ball is colliding with a solid tile
	bit     1, (ix + Object.BgColFlags)
	jr      z, UGZ3_CannonBall_MainLoop_NoCollision

	bit     6, (ix + Object.Flags04)
	jr      nz, +
	ld      a, SFX_BombBounce
	ld      (Sound_MusicTrigger1), a

+:	ld      e, (ix + Object.VelY)
	ld      d, (ix + Object.VelY + 1)
        ; jump if the object is moving up
	bit     7, d
	jr      nz, UGZ3_CannonBall_MainLoop_NoCollision
        ; subtract $80 from the current y speed
	ld      hl, $0080
	xor     a
	sbc     hl, de
	jr      c, +
	ld      hl, $0000
+:	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h

UGZ3_CannonBall_MainLoop_NoCollision:
        ; return if the cannon ball's x-position is < $DA0
	ld      h, (ix + Object.X + 1)
	ld      l, (ix + Object.X)
	ld      de, $0DA0
	xor     a
	sbc     hl, de
	ret     c
        ; return if the cannon ball's y-position is < $120
	ld      h, (ix + Object.Y + 1)
	ld      l, (ix + Object.Y)
	ld      de, $0120
	xor     a
	sbc     hl, de
	ret     c

        ; get a pointer to the pincer object
	ld      a, (UGZ3_Robotnik_PincersIndex)
	call    VF_Engine_GetObjectDescriptorPointer
	push    hl
	pop     iy

        ; if the pincer object is currently in state 2 display an explosion
	ld      a, (iy + Object.State)
	cp      $02
	jp      z, VF_Engine_DisplayExplosionObject

        ; jump if the pincer object's next state is 3
	ld      a, (iy + Object.StateNext)
	cp      $03
	jr      z, +

        ; jump if the cannon ball's current state is 3
	ld      a, (ix+$01)
	cp      $03
	jr      z, +

        ; set the pincer object's next state to 2
	ld      (iy + Object.StateNext), $02
	ld      (iy + UGZ3_Pincer_HurtStateCounter), $40

        ; decrement the pincer's hit counter
	dec     (iy + UGZ3_PincerHitCounter)
	jp      nz, VF_Engine_DisplayExplosionObject
        ; pincer hit counter is 0 - boss needs destroying
        ; get a pointer to the main boss object
	ld      a, (UGZ3_Robotnik_BossIndex)
	call    VF_Engine_GetObjectDescriptorPointer
	push    hl
	pop     iy
        ; change to state 7
	ld      (iy + Object.StateNext), $07
+:	jp      VF_Engine_DisplayExplosionObject
; ------------------------------------------------------------------------------


; =============================================================================
;  Robotnik Object Logic
; -----------------------------------------------------------------------------
UGZ3_Robotnik_State_00:		;$A483
.db $E0, $01
	.dw UGZ3_Robotnik_WaitForPlayerHeight
.db $FF, $08
	.db $17
.db $01, $01
	.dw VF_DoNothing
.db $FF, $02
	.dw UGZ3_Robotnik_SetInitialPosition
.db $FF, $05
	.db $01
.db $01, $01
	.dw VF_DoNothing
.db $FF, $03


UGZ3_Robotnik_WaitForPlayerHeight:		;$A49B
	ld      (ix + Object.FrameCounter), 224
        ; get player y-pos
	ld      hl, ($D514)
        ; subtract 528
	ld      de, $210
	xor     a
	sbc     hl, de
        ; if player y-pos >= 528 set FrameCounter = 1 (run logic on next frame)
	ret     c
	ld      (ix + Object.FrameCounter), $01
	ret     

UGZ3_Robotnik_State_MoveToPlayerX:		;$A4AE
.db $04, $03
	.dw UGZ3_Robotnik_MoveTowardsPlayerX
.db $04, $04
	.dw UGZ3_Robotnik_MoveTowardsPlayerX
.db $FF, $00

UGZ3_Robotnik_State_MoveToGrabPlayer:		;$A4B8
.db $04, $03
	.dw UGZ3_Robotnik_MoveToGrabPlayer
.db $04, $04
	.dw UGZ3_Robotnik_MoveToGrabPlayer
.db $FF, $00

UGZ3_Robotnik_State_MoveOverLedge:		;$A4C2
.db $E0, $02
	.dw UGZ3_Robotnik_MoveOverLedge
.db $FF, $00

UGZ3_Robotnik_State_MovePlayerDown:		;$A4C8
.db $E0, $02
	.dw UGZ3_Robotnik_MovePlayerDown
.db $FF, $00

UGZ3_Robotnik_State_FlyAway:		;$A4CE
.db $04, $03
	.dw UGZ3_Robotnik_FlyAway
.db $04, $04
	.dw UGZ3_Robotnik_FlyAway
.db $FF, $00

UGZ3_Robotnik_State_SpawnCannonBall:		;$A4D8
.db $10, $03
	.dw VF_DoNothing
.db $FF, $06		;spawn a cannon ball
	.db Object_UGZ_CannonBall
	.dw $0000
	.dw $0018
	.db $00
.db $E0, $04
	.dw VF_DoNothing
.db $FF, $00

UGZ3_Robotnik_State_DropOntoPincers:		;$A4EA
.db $E0, $01
	.dw UGZ3_Robotnik_DropOntoPincers
.db $FF, $00

UGZ3_Robotnik_State_08:		;$A4F0
.db $20, $01
	.dw VF_DoNothing
.db $FF, $05                ; change to state 9
	.db $09
.db $10, $01
	.dw VF_DoNothing
.db $FF, $00

UGZ3_Robotnik_State_09:		;$A4FD
.db $08, $03
	.dw UGZ3_Robotnik_State_09_Logic_01
.db $08, $04
	.dw UGZ3_Robotnik_State_09_Logic_01
.db $08, $03
	.dw UGZ3_Robotnik_State_09_Logic_01
.db $FF, $06	;spawn an explosion object
	.db Object_Explosion
	.dw $FFFC	;offset from current object's hpos
	.dw $FFF0	;offset from current object's vpos
	.db $00	
.db $08, $04
	.dw UGZ3_Robotnik_State_09_Logic_01
.db $08, $03
	.dw UGZ3_Robotnik_State_09_Logic_01
.db $FF, $06	;spawn an explosion object
	.db Object_Explosion
	.dw $0004
	.dw $FFF8
	.db $00
.db $08, $04
	.dw UGZ3_Robotnik_State_09_Logic_01
.db $FF, $00

;**************************
;*	lift player 
UGZ3_Robotnik_State_LiftPlayer:		;$A527
.db $E0, $02
	.dw UGZ3_Robotnik_LiftPlayer
.db $FF, $00

;**************************
;*	grab player
UGZ3_Robotnik_State_GrabPlayer:		;$A52D
.db $E0, $02
	.dw UGZ3_Robotnik_GrabPlayer
.db $FF, $00


UGZ3_Robotnik_SetInitialPosition:		;A553
        ; work out what the object's index number is
	push    ix
	pop     hl
	call    VF_Engine_GetObjectIndexFromPointer
        ; store it for later
	ld      (UGZ3_Robotnik_BossIndex), a

	set     7, (ix + Object.Flags03)
        ; get horizontal camera offset
	ld      hl, (Camera_X)
	ld      de, $0120
	add     hl, de
        ; set the object's x-pos to camera.x + $120
	ld      (ix+$11), l
	ld      (ix+$12), h
	ret     

UGZ3_Robotnik_MoveTowardsPlayerX:		;$A54E
        ; lock the camera
	call    VF_Engine_SetMinimumCameraX
        ; move robotnik towards the player
	ld      de, $0000
	ld      hl, (Player.X)
	ld      bc, $06C0
	call    VF_Logic_MoveHorizontalTowardsObject
	call    VF_Engine_UpdateObjectPosition

        ; set robotnik's y-pos to 80 pixels from top of the screen
	ld      hl, (Camera_Y)
	ld      de, $0050
	add     hl, de
	ld      (ix+$14), l
	ld      (ix+$15), h

        ; get the horizontal speed
	ld      a, (ix+$16)
	ld      h, (ix+$17)
        ; bail out if robotnik is still moving to the player's position
	or      h
	ret     nz

        ; change to state 2
	ld      (ix+$02), $02
        ; play the boss music
	ld      a, Music_Boss
	ld      (Sound_MusicTrigger1), a
	ret     


UGZ3_Robotnik_MoveToGrabPlayer		;$A57F
	call    VF_Engine_SetMinimumCameraX

        ; set robotnik's position above the player
	ld      hl, (Player.X)
	ld      (ix + Object.X), l
	ld      (ix + Object.X + 1), h
        ; set robotnik's vertical speed
	ld      hl, $0E00
	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h
	call    VF_Engine_UpdateObjectPosition
        
        ; is robotnik colliding with the player?
	call    VF_Engine_CheckCollision
	ld      a, (ix + Object.SprColFlags)
	and     $0f
	ret     z
        ; we are colliding so change to the grab state
	ld      (ix + Object.StateNext), $0B
	ret     

;****************************************************
;*  Deals with the part where Robotnik grabs sonic. *
;****************************************************
UGZ3_Robotnik_GrabPlayer:		;$A5A5
        ; match the player's X position
	ld      hl, (Player.X)
	ld      (ix + Object.X), l
	ld      (ix + Object.X + 1), h

        ; set y position to 32 pixels above the player
	ld      hl, (Player.Y)
	ld      de, $FFE0
	add     hl, de
	ld      (ix + Object.Y), l
	ld      (ix + Object.Y + 1), h

        ; return if robotnik's Y position is < $360
	ld      de, $0360
	xor     a
	sbc     hl, de
	ret     c

        ; we've reached the required Y coordinate so change to the next state
	ld      (ix+$02), $0A
        ; change the player's state to "grabbed by robotnik" 
	ld      a, PlayerState_UGZ_Boss
	ld      (Player.StateNext), a
        ; stop moving down
	ld      hl, $0000		;set vertical velocity to 0
	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h
	ret

;****************************************************
;*  Deals with the part where Robotnik lifts sonic. *
;****************************************************
UGZ3_Robotnik_LiftPlayer:		;$A5D5
	call    VF_Engine_SetMinimumCameraX
        ;start moving directly up
	ld      hl, $FE00
	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h
	ld      hl, $0000
	ld      (ix + Object.VelX), l
	ld      (ix + Object.VelX + 1), h
	call    VF_Engine_UpdateObjectPosition

        ; move the player to match
	ld      de, $0000
	ld      bc, $0020
	call    UGZ3_Robotnik_SetPlayerPosition

        ; return if robotnik's y-pos > 96
	ld      e, (ix + Object.Y)
	ld      d, (ix + Object.Y + 1)
	ld      hl, $0060
	xor     a
	sbc     hl, de
	ret     c

        ; change to the next state
	ld      (ix + Object.StateNext), $03

        ; not sure why this is here but it's a nice easter egg.
        ; if the level timer is at 9 minutes and the player is pressing down+2
        ; sonic falls out of robotnik's grabs into the lava.
        ; If the player presses right fast enough to land on the ledge he can
        ; jump over the boss and run straight to the end of the level.
	ld      a, (Engine_InputFlags)
        cp      (BTN_DOWN | BTN_2)
	ret     nz
	ld      a, (LevelTimer_Minutes)
	cp      $09
	ret     nz
	ld      a, PlayerState_Falling
	ld      (Player.StateNext), a
	ld      (ix + Object.StateNext), $05
	ret     


UGZ3_Robotnik_MoveOverLedge:		;$A61D
	call    VF_Engine_SetMinimumCameraX
        ; set horizontal velocity
	ld      hl, $0200
	ld      (ix + Object.VelX), l
	ld      (ix + Object.VelX + 1), h
        ; set vertical velocity
	ld      hl, $0000
	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h
	call    VF_Engine_UpdateObjectPosition
        ; move the player to match
	ld      de, $0000
	ld      bc, $0020
	call    UGZ3_Robotnik_SetPlayerPosition

        ; return if robotnik's x-pos is < $D50
	ld      e, (ix + Object.X)
	ld      d, (ix + Object.X + 1)
	ld      hl, $0D50
	xor     a
	sbc     hl, de
	ret     nc

        ; change to the next state
	ld      (ix + Object.StateNext), $04
        ; stop moving horizontally
	ld      hl, $0000
	ld      (ix + Object.VelX), l
	ld      (ix + Object.VelX + 1), h
	ret     


UGZ3_Robotnik_MovePlayerDown:		;$A659
	call    VF_Engine_SetMinimumCameraX
        ; move down
	ld      hl, $0200
	ld      (ix + Object.VelY), l		;set vertical velocity
	ld      (ix + Object.VelY + 1), h
	call    VF_Engine_UpdateObjectPosition
        ; move the player to match
	ld      de, $0000
	ld      bc, $0020
	call    UGZ3_Robotnik_SetPlayerPosition

        ; return if robotnik's y-pos is < $B8
	ld      e, (ix + Object.Y)
	ld      d, (ix + Object.Y + 1)
	ld      hl, $00B8
	xor     a
	sbc     hl, de
	ret     nc

        ; change to the next state
	ld      (ix + Object.StateNext), $05

        ; stop robotnik moving
	ld      hl, $0000
	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h

        ; make sonic fall
	ld      a, PlayerState_Falling
	ld      (Player.StateNext), a

        ; lock the camera on the boss field
	ld      bc, $0CCA
	ld      de, $0078
	call    VF_Engine_SetCameraAndLock
	ret


UGZ3_Robotnik_FlyAway:		;$A69A
        ; start flying away
	ld      hl, $0CA0
	ld      de, $0000
	ld      bc, $0200
	call    VF_Logic_MoveHorizontalTowardsObject
	ld      hl, $0020
	ld      de, $0000
	ld      bc, $0200
	call    VF_Logic_MoveVerticalTowardsObject
	call    VF_Engine_UpdateObjectPosition

        ; wait until robotnik has reached the desired position
	ld      a, (ix + Object.VelX)
	ld      h, (ix + Object.VelX + 1)
	or      h
	ret     nz
	ld      a, (ix + Object.VelY)
	ld      h, (ix + Object.VelY + 1)
	or      h
	ret     nz

        ; change to the next state
	ld      (ix+$02), $06
	ret     


UGZ3_Robotnik_DropOntoPincers:		;$A6CA
	res     7, (ix + Object.Flags03)

        ; check player collisions
	call    VF_Engine_CheckCollision

        ; check collisions with background tiles
	call    LABEL_200 + $60

        ; set y-speed to $200
	ld      hl, $0200
	ld      (ix + Object.VelY), l
	ld      (ix + Object.VelY + 1), h

        ; if robotnik has hit the floor start moving forward
	ld      hl, $0000
	bit     1, (ix + Object.BgColFlags)
	jr      z, +
	ld      hl, $0280
+:	ld      (ix + Object.VelX), l
	ld      (ix + Object.VelX + 1), h
	call    VF_Engine_UpdateObjectPosition

        ; return if robotnik's x-pos < $DB0
	ld      h, (ix + Object.X + 1)
	ld      l, (ix + Object.X)
	ld      de, $0DB0
	xor     a
	sbc     hl, de
	ret     c

        ; change to the next state
	ld      (ix + Object.StateNext), $08
        ; get a pointer to the pincer object
	ld      a, (UGZ3_Robotnik_PincersIndex)
	call    VF_Engine_GetObjectDescriptorPointer
	push    hl
	pop     iy
        ; set the pincer object's state to 3
	ld      (iy + Object.StateNext), $03
	ret     

UGZ3_Robotnik_State_09_Logic_01:		;$A711
	ld      b, (ix+$1E)
	ld      a, (ix+$1F)
	and     $01
	rlca    
	rlca    
	rlca    
	rlca    
	sub     $08
	add     a, b
	jr      nz, +
	dec     (ix+$1F)
+:	ld      (ix+$1E), a
	ld      l, a
	ld      h, $00
	ld      de, $FE80
	add     hl, de
	ld      (ix+$18), l		;set vertical velocity
	ld      (ix+$19), h
	ld      a, (ix+$1E)
	add     a, $80
	ld      l, a
	ld      h, $00
	ld      de, $FF00
	add     hl, de
	ld      (ix+$16), l		;set horizontal velocity
	ld      (ix+$17), h
	call    VF_Engine_UpdateObjectPosition
	ret     

; =============================================================================
;  UGZ3_Robotnik_SetPlayerPosition(int16 offsetx, int16 offsety)
; -----------------------------------------------------------------------------
;  Desc: Sets the player's position to an offset of Robotnik's position
;  In:
;       BC  - vertical offset
;       DE  - horizontal offset
;  Out:
;       None
; -----------------------------------------------------------------------------
UGZ3_Robotnik_SetPlayerPosition:		;$A74B
	ld      l, (ix + Object.X)
	ld      h, (ix + Object.X + 1)
	add     hl, de
	ld      (Player.X), hl
	ld      l, (ix + Object.Y)
	ld      h, (ix + Object.Y + 1)
	add     hl, bc
	ld      (Player.Y), hl
	ret
; -----------------------------------------------------------------------------


; =============================================================================
;  Pincer Logic
; -----------------------------------------------------------------------------
Logic_UGZ3_Pincers:			;$A760
.dw UGZ3_Pincers_State_Init
.dw UGZ3_Pincers_State_MainLoop
.dw UGZ3_Pincers_State_Hurt
.dw UGZ3_Pincers_State_Destroy

UGZ3_Pincers_State_Init:		;$A768
.db $FF, $02
	.dw UGZ_Pincers_Init
.db $FF, $05
	.db $01
.db $E0, $00
	.dw VF_DoNothing
.db $FF, $03

UGZ3_Pincers_State_MainLoop:		;$A775
.db $FF, $07
	.dw UGZ3_Pincers_SetAnimation1
	.dw LABEL_200 + $1B     ; test object collisions
.db $FF, $07
	.dw UGZ3_Pincers_SetAnimation2
	.dw LABEL_200 + $1B     ; test object collisions
.db $FF, $00

UGZ3_Pincers_State_Hurt:		;$A783:
.db $08, $03
	.dw UGZ3_Pincers_Hurt
.db $08, $05
	.dw UGZ3_Pincers_Hurt
.db $FF, $00

UGZ3_Pincers_State_Destroy:		;$A78D
.db $FF, $02
	.dw VF_Score_AddBossValue
.db $08, $04
	.dw VF_DoNothing
.db $FF, $06	;spawn an explosion sprite
	.db Object_Explosion
	.dw $000C
	.dw $FFEC
	.db $05
.db $08, $04
	.dw VF_DoNothing
.db $FF, $06	;spawn an explosion sprite
	.db Object_Explosion
	.dw $FFF4
	.dw $FFEC
	.db $05
.db $08, $04
	.dw VF_DoNothing
.db $FF, $06	;spawn an explosion sprite
	.db Object_Explosion
	.dw $0004
	.dw $FFE8
	.db $05
.db $08, $04
	.dw VF_DoNothing
.db $FF, $06	;spawn an explosion sprite
	.db Object_Explosion
	.dw $FFFC
	.dw $FFE8
	.db $05
.db $08, $04
	.dw VF_DoNothing
.db $FF, $06	;spawn an explosion sprite
	.db Object_Explosion
	.dw $0000
	.dw $FFE4
	.db $05
.db $E0, $00
	.dw VF_DoNothing
.db $08, $00
	.dw UGZ3_Pincers_Destroy
.db $FF, $00

UGZ_Pincers_Init:		;$A7D7
        ; store the index of the pincer object so that it's available forr the
        ; other boss objects (cannon balls)
	push    ix
	pop     hl
	call    VF_Engine_GetObjectIndexFromPointer
	ld      (UGZ3_Robotnik_PincersIndex), a
        ; set up the default hit counter value
	ld      (ix + UGZ3_PincerHitCounter), UGZ3_PincerNumberOfHits 
	ret     

UGZ3_Pincers_SetAnimation1:		;$A7E5
        ; set animation frame to $01
	ld      (ix + Object.AnimFrame), $01
	jp      +

UGZ3_Pincers_SetAnimation2:		;$A7EC
        ; set animation frame to $02
	ld      (ix + Object.AnimFrame), $02	

+	; set the animation speed based on the number of hits remaining
        ld      a, (ix + UGZ3_PincerHitCounter)
	inc     a
	add     a, a
	ld      (ix + Object.FrameCounter), a
	ret     

UGZ3_Pincers_Hurt:		;$A7F9
        ; return if the next state has been set to 3
	ld      a, (ix + Object.StateNext)
	cp      $03
	ret     z

        ; test object collisions
	call    LABEL_200 + $1B
	dec     (ix + UGZ3_Pincer_HurtStateCounter)
	ret     nz

	ld      (ix + Object.StateNext), $01
	ret     

UGZ3_Pincers_Destroy:		;$A80B
        ; set some flags - unknown for now
	call    LABEL_200 + $F3
        ; destroy this object
	ld      (ix + Object.ObjID), Object_Dummy
	ret


; BASE $A7C6
.dw $8A2F

.db $04     ; channels
.db 0
.db $03     ; tempo ( 3 = 120 BPM)
.db 0


; CHANNEL 0 HEADER
.dw Channel0_Data   ; channel data ptr
.db $FF     ; global pitch adjustment
.db $05    ; global volume adjustment

; CHANNEL 1 HEADER
.dw Channel1_Data   ; channel data ptr
.db $FF     ; global pitch adjustment
.db $05     ; global volume adjustment

; CHANNEL 2 HEADER
.dw Channel2_Data
.db $FF
.db $01

; CHANNEL 3 HEADER (noise)
.dw Channel3_Data
.db 0
.db 0


Channel0_Data:
    VolumeEnvelope  5
    A5
        .db 3
    F5
    A5
    F5
    
    B5
    G5
    B5
    G5
    
    C6
    A5
    C6
    A5
    
    D6
    B5
    D6
    B5
;--------------------------------------
Mus_Test_Ch0_L0:
    C6
    B5
    A5
    G5
    Loop    1, 16, Mus_Test_Ch0_L0
;--------------------------------------
    Rest
        .db 9
    C6
        .db 3
    C6
    A5
        .db 6
    C6
        .db 3
;--------------------------------------
    B5
        .db 6
    C6
        .db 3
    B5
        .db 6
        
    AdjustVolume    2
    G5
        .db 3
    A5
    C6
    A5
    AdjustVolume    -2
;--------------------------------------
    Rest 
        .db 9
    A5
        .db 3
    E6
    D6
        .db 6
    C6
        .db 3
;--------------------------------------
    B5
        .db 6
    C6
        .db 3
    B5
        .db 6
    AdjustVolume    2
    G5
        .db 3
    A5
    C6
    E6
    AdjustVolume    -2
;--------------------------------------
    Stop

Channel2_Data:
    VolumeEnvelope  6
    Rest
        .db 48
    G4
        .db 9
    F4
    G4
    F4
    
    G4
        .db 6
    F4
    A4
        .db 9
    G4
    F4
        .db 30
; -------------------------------------
    VolumeEnvelope 6
    F4
        .db 9
    G4
    A4
        .db 6
    F4
        .db 9
    G4
    A4
        .db 6
    A4
        .db 9
    G4
        .db 27
; -------------------------------------
    Stop
Mus_Test_Ch2_L0:
    Rest
        .db 6
    E4
        .db 9
    E4
; -------------------------------------
    D4
    D4
        .db 12
    Rest
        .db 3
; -------------------------------------
    Loop    1, 1, Mus_Test_Ch2_L0
; -------------------------------------
    Stop
    

; ------------------------------------- 
Channel3_Data:
    Stop


Channel1_Data:
    Rest
        .db 48
    VolumeEnvelope  12
; -------------------------------------
Mus_Test_C1_L1:
    Branch          Mus_Test_Ch1_Bass0
    Loop            1, 3, Mus_Test_C1_L1
; -------------------------------------
    C4
    C4
    A3
    A3
    As3
    As3
    B3
    B3
; -------------------------------------
Mus_Test_C1_L2:
    Branch          Mus_Test_Ch1_Bass0
    Loop            1, 3, Mus_Test_C1_L2
; -------------------------------------
    C3
    C3
    C3
    C3
    C3
    C3
    D3
    E3
; -------------------------------------
    F3
    F3
    F3
    F3
    F3
    F3
    F3
    F3
; -------------------------------------
    E3
    E3
    E3
    E3
    E3
    C3
    D3
    E3
; -------------------------------------
    PitchAdjust     -7
    Branch          Mus_Test_Ch1_Bass0
    PitchAdjust     7
; -------------------------------------
    E3
    E3
    E3
    E3
    E3
    C3
    D3
    E3
; -------------------------------------
    Stop

Mus_Test_Ch1_Bass0:
    C4
        .db 3
    C4
    C4
    C4
    C4
    C4
    C4
    C4
    Return

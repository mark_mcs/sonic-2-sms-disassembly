#!/bin/bash
WLA_HOME=wla_dx_binaries_latest

rm -f s2.{o,sym,gg} 
rm -f compare.txt

$WLA_HOME/wla-z80 -vo src/s2.asm s2.o > build_report.txt
[ $? -ne 0 ] && echo "Error while assembling." && exit -1

$WLA_HOME/wlalink -rs link.txt s2.sms
[ $? -ne 0 ] && echo "Error while linking." && exit -1

cmp -s s2.sms Sonic\ the\ Hedgehog\ 2\ \(UE\)\ \[!\].sms
if [ $? -eq 0 ]; then
    echo "Assembled ROM matches original"
else
    cmp -l s2.sms Sonic\ the\ Hedgehog\ 2\ \(UE\)\ \[!\].sms | awk '{printf("%X : %02X    %02X   \n", $1, strtonum(0$2), strtonum(0$3))}' > compare.txt
    echo "Assembled ROM does NOT match original!"
    exit -2
fi
